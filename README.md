# Core

Core library for algodynamics virtual interactive labs. Gives access to default styles, analytics via port and undo/redo functionalities. 